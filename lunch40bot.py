#!/usr/bin/env python

import json, urllib.request, requests
import datetime, locale
import re

# prepare title
message = '---\n#### Hey there, here comes the list of volunteers preparing lunch this week' + '\n\n'

# read from org file stored in a git repo
data = urllib.request.urlopen('https://git.rwth-aachen.de/schneider/lunch4.0/-/raw/master/lunch40.org').readlines()
data = list(map(lambda x : x.decode('utf8'),data))

# get the current calendar week
today = datetime.date.today()
yearday = today.timetuple().tm_yday
first_day_of_year = datetime.date.weekday(datetime.datetime(today.year,1,1))+1
cw = round((yearday + first_day_of_year)/7 + 0.5)

# fetch the header
header = data[0]
# match all lines starting with the current calendarweek
cw_lines = list(filter(lambda x: re.match(r'.*' + str(cw),x), data))

# compose the message such that it appears as code
message += '```\n'
message += header + ''.join(cw_lines)
message += '```\n'

# provide data
mm_data = {"username": "lunchbot", "text": message}

# url to mattermost incoming webhook
# this is a secret here. Please enter something useful
mm_url = SECRET

print("Sending message to mattermost")

# gogogo!
r = requests.post(mm_url, data=json.dumps(mm_data))
